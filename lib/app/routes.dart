// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:verification_code/bussiness_logic/bloc/login_bloc.dart';
import '../presentation/pages/private_area/dashboard_screen.dart';
import '../presentation/pages/public_area/code_screen.dart';

class Routes {
  // Route name constants
  static const String Code = '/code';
  static const String Dashboard = '/dashboard';

  Map<String, WidgetBuilder> getRoutes() {
    return {
      Routes.Code: (context) => BlocProvider(
            create: (context) => LoginBloc(),
            child: const CodeScreen(),
          ),
          
      Routes.Dashboard: (context) => const DashboardScreen() ,
    };
  }
}
