import 'package:flutter/material.dart';
import 'package:verification_code/presentation/widgets/custumed_button.dart';

import '../../../app/routes.dart';
import '../../styles/text_styles.dart';

class SmsScreen extends StatelessWidget {
  final String phone;
  const SmsScreen({required String this.phone, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Stack(
            fit: StackFit.expand,
            children: [
              const Positioned(top: 40, left: 30, child: Icon(Icons.close)),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 110, bottom: 30),
                    child: Text("קוד אימות", style: TextStyles.extraBold34Text),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 70),
                    child: Text(
                      "שלחנו הודעה עם קוד חד פעמי לטלפון הנייד שלך",
                      style: TextStyles.extraBold10subtitle,
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(bottom: 30),
                      child: Text(_getSecuredPhone(),
                          style: TextStyles.regularText)),
                  CustumedButton(
                      onPressed: () => Navigator.of(context).pushNamed(Routes.Code),
                      text: "שליחת קוד אימות")
                ],
              ),
            ],
          ),
        ));
  }

  _getSecuredPhone() => phone.replaceRange(3, 7, "-****");
}
