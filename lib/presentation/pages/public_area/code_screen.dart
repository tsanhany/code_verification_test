import 'dart:developer';

import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:verification_code/bussiness_logic/bloc/login_bloc.dart';

import '../../../app/routes.dart';
import '../../styles/text_styles.dart';
import '../../widgets/custumed_button.dart';

class CodeScreen extends StatefulWidget {
  const CodeScreen({Key? key}) : super(key: key);

  @override
  State<CodeScreen> createState() => _CodeScreenState();
}

class _CodeScreenState extends State<CodeScreen> {
  final TextEditingController _codeController = TextEditingController();
  final CountDownController _countDownController = CountDownController();
  bool isTimeExceeded = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => dissmissKeyboard(context),
      child: Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: false,
          body: SafeArea(
            child: Stack(
              fit: StackFit.expand,
              children: [
                const Positioned(top: 40, left: 30, child: Icon(Icons.close)),
                SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    reverse: false,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 110, bottom: 30),
                          child: Text("קוד אימות",
                              style: TextStyles.extraBold34Text),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 50, left: 20, right: 20),
                          child: Text(
                            "נא להזין את הקוד שנשלח אליך במסרון או בהודעה קולית לטלפון הנייד שלך",
                            textAlign: TextAlign.center,
                            style: TextStyles.extraBold10subtitle,
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: _pinCodeField()),
                        _continueButton(context),
                        _sendAgain(),
                        _timerSection()
                      ],
                    )),
              ],
            ),
          )),
    );
  }

  _pinCodeField() => BlocBuilder<LoginBloc, LoginState>(
        buildWhen: (prevState, state) =>
            state is LoginInitial ||
            state is CodeChangedState ||
            state is CodeSubmittedState,
        builder: (context, state) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 4),
            child: Column(
              children: [
                PinFieldAutoFill(
                  autoFocus: true,
                  currentCode: state.code,
                  decoration: BoxLooseDecoration(
                      strokeColorBuilder: PinListenColorBuilder(
                        state.codeInvalid ? Colors.transparent : Colors.green,
                        state.codeInvalid ? Colors.transparent : Colors.green,
                      ),
                      bgColorBuilder: state.codeInvalid
                          ? PinListenColorBuilder(
                              const Color.fromARGB(255, 252, 145, 138),
                              const Color.fromARGB(255, 252, 145, 138))
                          : null,
                      textStyle: TextStyle(
                          color: state.codeInvalid
                              ? const Color.fromARGB(255, 124, 28, 21)
                              : Colors.green,
                          fontSize: 24,
                          height: 1.5),
                      radius: const Radius.circular(15.0),
                      obscureStyle: ObscureStyle(
                        isTextObscure: state.codeInvalid ? false : true,
                        obscureText: '*',
                      )),
                  controller: _codeController,
                  onCodeChanged: (code) {
                    if (code?.length == 6) {
                      dissmissKeyboard(context);
                    }
                    context.read<LoginBloc>().add(CodeChangedEvent(code));
                  },
                  codeLength: 6,
                ),
                Visibility(
                    visible: state.codeInvalid,
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    child: const Text(
                      "קוד שגוי",
                      style: TextStyle(color: Colors.red, height: 2.6),
                    ))
              ],
            ),
          );
        },
      );

  Widget _continueButton(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previousState, state) =>
          state is LoginInitial ||
          (state is CodeChangedState && previousState is! TimeExceededState) ||
          state is TimeExceededState ||
          state is SendAgainState,
      builder: (context, state) {
        log("continueButton rendered -> isTimeExceeded: $isTimeExceeded. state: ${state.runtimeType}");
        return CustumedButton(
            isEnabled: isTimeExceeded ? false : state.isNextEnabled,
            onPressed: () => context
                .read<LoginBloc>()
                .add(CodeSubmittedEvent(_codeController.text)),
            text: "המשך");
      },
    );
  }

  Widget _sendAgain() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        textDirection: TextDirection.rtl,
        children: [
          const Text(
            "?לא קיבלתי",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
          ),
          const SizedBox(
            width: 20,
          ),
          TextButton(
              onPressed: () {
                log("sendAgainEvent() triggered");
                return context
                    .read<LoginBloc>()
                    .add(SendAgainEvent(_codeController.text));
              },
              child: const Text("שלח לי שוב",
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      color: Colors.green)))
        ],
      ),
    );
  }

  Widget _timerSection() {
    return BlocListener<LoginBloc, LoginState>(
      listenWhen: (_, state) =>
          state is SendAgainState || state is CodeSubmittedState,
      listener: (context, state) {
        if (state is CodeSubmittedState) {
          if (state.codeInvalid) {
            return;
          } else {
            Navigator.of(context).pushNamed(Routes.Dashboard);
          }
        }

        log("count controller restarting");
        _countDownController.restart();
      },
      child: CircularCountDownTimer(
        duration: 20,
        initialDuration: 0,
        controller: _countDownController,
        width: MediaQuery.of(context).size.width / 3,
        height: MediaQuery.of(context).size.height / 4,
        ringColor: Colors.transparent,
        fillColor: Colors.green,
        strokeWidth: 9.0,
        strokeCap: StrokeCap.round,
        textStyle: const TextStyle(
            fontSize: 21.0, color: Colors.green, fontWeight: FontWeight.bold),
        textFormat: CountdownTextFormat.MM_SS,
        isReverse: false,
        isReverseAnimation: false,
        isTimerTextShown: true,
        autoStart: true,
        onStart: () {
          debugPrint('Countdown Started');
          isTimeExceeded = false;
        },
        onComplete: () {
          if (!isTimeExceeded) {
            log("count controller -> OnComplete()");
            isTimeExceeded = true;
            context.read<LoginBloc>().add(TimeExceededEvent());
          }
        },
      ),
    );
  }

  dissmissKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }
}
