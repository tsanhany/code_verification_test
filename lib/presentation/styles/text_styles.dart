import 'package:flutter/material.dart';

class TextStyles {
  static TextStyle extraBold34Text = const TextStyle(
    fontFamily: "OpenSans",
    fontWeight: FontWeight.w700,
    fontSize: 34
  );
  
  static TextStyle extraBold10subtitle = const TextStyle(
    fontFamily: "OpenSans",
    fontWeight: FontWeight.w700,
    fontSize: 14
  );

  static TextStyle regularText = const TextStyle(
    fontFamily: "OpenSans",
    fontWeight: FontWeight.w300,
    fontSize: 34
  );
}