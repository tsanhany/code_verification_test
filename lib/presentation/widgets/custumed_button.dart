import 'package:flutter/material.dart';

class CustumedButton extends StatelessWidget {
  final Function()? onPressed;
  final String text;
  final bool isEnabled;
  const CustumedButton({required this.onPressed, required this.text, this.isEnabled = true, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textStyle = isEnabled ? const TextStyle(color: Colors.white) : const TextStyle(color: Colors.black);
    return TextButton(
                  style: ButtonStyle(
                    splashFactory: NoSplash.splashFactory,
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(19.0), )),
                    backgroundColor: isEnabled ? MaterialStateProperty.all<Color>(Colors.green[900]!) : null,
                    elevation: isEnabled ? null : MaterialStateProperty.all<double>(1)),
                  onPressed: isEnabled ? onPressed : null,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 2),
                    child: Text(text, style: textStyle),
                  ));
  }
}