import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:verification_code/presentation/pages/public_area/sms_screen.dart';
import 'app/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    return MaterialApp(
      title: 'Verification Code',
      routes: Routes().getRoutes(),
      theme: ThemeData(
        fontFamily: "OpenSans",
      ),
      home: const SmsScreen(
        phone: '0546667431',
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
