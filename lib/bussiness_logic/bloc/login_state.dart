part of 'login_bloc.dart';

@immutable
abstract class LoginState {
  final String code;
  final bool codeInvalid;
  final bool isNextEnabled;
  const LoginState(
      {required this.codeInvalid, required this.isNextEnabled, this.code = ""});
}

class LoginInitial extends LoginState {
  const LoginInitial() : super(codeInvalid: false, isNextEnabled: false);
}

class CodeChangedState extends LoginState {
  const CodeChangedState({required bool isNextEnabled, required String code})
      : super(codeInvalid: false, isNextEnabled: isNextEnabled, code: code);
}

class CodeSubmittedState extends LoginState {
  const CodeSubmittedState({required bool codeInvalid, required String code})
      : super(codeInvalid: codeInvalid, isNextEnabled: true, code: code);
}

class SendAgainState extends LoginState {
  const SendAgainState({required bool isNextEnabled}) : super(codeInvalid: false, isNextEnabled: isNextEnabled);
}

class TimeExceededState extends LoginState {
  const TimeExceededState() : super(codeInvalid: true, isNextEnabled: false);
}
