part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class CodeChangedEvent extends LoginEvent {
  final String? code;
  CodeChangedEvent(this.code);
}

class CodeSubmittedEvent extends LoginEvent {
  final String code;
  CodeSubmittedEvent(this.code);
}

class SendAgainEvent extends LoginEvent {
  final String code;
  SendAgainEvent(this.code);
}

class TimeExceededEvent extends LoginEvent {}
