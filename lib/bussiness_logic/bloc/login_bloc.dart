import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(const LoginInitial()) {
    on<CodeChangedEvent>((event, emit) {
      emit(CodeChangedState(
          isNextEnabled: (event.code?.length == 6), code: event.code ?? ""));
    });

    on<CodeSubmittedEvent>((event, emit) {
      _mapSubmittedEvent(event, emit);
    });

    on<SendAgainEvent>((event, emit) {
      // on SendAgainEvent code is deleted,
      emit(SendAgainState(isNextEnabled: (event.code.length == 6) ));
    });

    on<TimeExceededEvent>((event, emit) {
      emit(const TimeExceededState());
    });
  }

  _mapSubmittedEvent(CodeSubmittedEvent event, Emitter emit) {
    final codeInvalid = (event.code != "123456");
    emit(CodeSubmittedState(codeInvalid: codeInvalid, code: event.code));
  }
}
